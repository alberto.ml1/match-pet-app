import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MypetRoutingModule } from './mypet-routing.module';
import { MypetComponent } from './mypet-component/mypet.component';


@NgModule({
  declarations: [MypetComponent],
  imports: [
    CommonModule,
    MypetRoutingModule
  ]
})
export class MypetModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MypetComponent } from './mypet-component/mypet.component';

const routes: Routes = [{
  path: '', component: MypetComponent,
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MypetRoutingModule { }
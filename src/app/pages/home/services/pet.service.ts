import { IMyPet } from './../models/IMyPet';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { from, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PetService {
  private animalUrl: string = 'http://localhost:3000/getListAnimal';

  constructor(private http: HttpClient) {}
    public getAnimal(): Observable<IMyPet> {
    return this.http.get(this.animalUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
   catchError((err) => {
     throw new Error(err.message);
   })
 );
  }
  public getAnimalDetail(id: string): Observable<IMyPet> {
    return this.http.get(`${this.animalUrl}/${id}`).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
   catchError((err) => {
     throw new Error(err.message);
   })
 );
  }
  public postDataAnimal(iMyPet:IMyPet):Observable<IMyPet>{
    return this.http.post(this.animalUrl, iMyPet).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),

      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}

export interface IMyPet{
    specie: "Dog" | "Cat" | "Other";
    race: string;
    petname: string;
    gender: "Male" | "Female";
    age: number;
    city: string;
    image: string;
    email: string;
    id: number;
}
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IMyPet } from '../../models/IMyPet';
import { PetService } from '../../services/pet.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  public signuppetForm: FormGroup;
  public datapet?: IMyPet;
  public submitted: boolean = false;

  constructor(
    private _builder: FormBuilder,
    private petService: PetService
  ) { this.signuppetForm = this._builder.group({
    specie: ['', [Validators.required, Validators.maxLength(15)]],
     race: ['', Validators.required],
     petname: ['', Validators.required],
     gender: ['', Validators.required],
     age: ['', [Validators.required, Validators.max(25), Validators.min(0)]],
     city: ['', Validators.required],
     image: ['', Validators.required],
     email: ['', Validators.compose([Validators.email, Validators.required])],
   });
}

  ngOnInit(): void {
  }
  public onSubmit(): void {
     this.submitted= true;
     if (this.signuppetForm.valid){
         this.postToAnimal();
       this.signuppetForm.reset();
       this.submitted= false;
     }
   }
   public postToAnimal(): void{
    
    const imypet: IMyPet= {
      id: Math.floor(Math.random() * 10000) + 1,
      specie: this.signuppetForm.get('specie')?.value,
      race: this.signuppetForm.get('race')?.value,
      petname: this.signuppetForm.get('petname')?.value,
      gender: this.signuppetForm.get('gender')?.value,
      age: parseInt(this.signuppetForm.get('age')?.value),
      city: this.signuppetForm.get('city')?.value,
      email: this.signuppetForm.get('email')?.value,  
      image: this.signuppetForm.get('image')?.value,   
    };
    this.petService.postDataAnimal(imypet).subscribe((data: any)=>{
      this.datapet= data;
      console.log(this.datapet);
      alert("Se han guardado los datos de su mascota")
    },
    (err: any)=>{console.error(err.message);}
    );
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatchRoutingModule } from './match-routing.module';
import { MatchComponent } from './match-component/match.component';
import { MatchDetailComponent } from './match-detail/match-detail.component';


@NgModule({
  declarations: [MatchComponent, MatchDetailComponent],
  imports: [
    CommonModule,
    MatchRoutingModule
  ]
})
export class MatchModule { }

import { PetService } from './../../home/services/pet.service';
import { Component, OnInit } from '@angular/core';
import { IMyPet } from '../../home/models/IMyPet';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-match-detail',
  templateUrl: './match-detail.component.html',
  styleUrls: ['./match-detail.component.scss']
})
export class MatchDetailComponent implements OnInit {
  public datapetdetaillist?: IMyPet; 
  public petId: string | null = '';
  constructor(private petService: PetService, private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {

    this.activatedRoute.paramMap.subscribe(params => {
      this.petId = params.get('id');
    })
    this.getAnimalDetailListData(); 
  }
  public getAnimalDetailListData(): void {
   if (this.petId) ( this.petService.getAnimalDetail(this.petId).subscribe(
      (data: any) => {
        this.datapetdetaillist = data;
        console.log(this.datapetdetaillist);
      },
      (err) => {
        console.error(err.message);
      }
   ));
  }
}

import { PetService } from './../../home/services/pet.service';
import { IMyPet } from './../../home/models/IMyPet';
import { Component, OnInit } from '@angular/core';
import { createInjectorType } from '@angular/compiler/src/render3/r3_injector_compiler';

@Component({
  selector: 'app-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.scss'],
})
export class MatchComponent implements OnInit {
  public datapetlist?: IMyPet[];
  constructor(private petService: PetService) {}

  ngOnInit(): void {
    this.getAnimalListData();
  }
  public getAnimalListData(): void {
    this.petService.getAnimal().subscribe(
      (data: any) => {
        this.datapetlist = data;
        console.log(this.datapetlist);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }
}

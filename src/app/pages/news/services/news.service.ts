import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { from, Observable, throwError } from 'rxjs';
import { INews } from '../models/INews';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  private newsUrl: string = 'http://localhost:3000/getnews';
  constructor(private http: HttpClient) { }
  public getNews(): Observable<INews> {
    return this.http.get(this.newsUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
   catchError((err) => {
     throw new Error(err.message);
   })
 );
  }
}

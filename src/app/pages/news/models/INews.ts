export interface INews{
  id: number;
  title: string;
  subtitle: string;
  notice: string;
  image: string;
}
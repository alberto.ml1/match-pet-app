import { NewsService } from './../services/news.service';
import { Component, OnInit } from '@angular/core';
import { INews } from '../models/INews';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  public datanews?: INews[];
  constructor(private newsService: NewsService) { }

  ngOnInit(): void {
    this.getNewsData();
  }
  public getNewsData(): void {
    this.newsService.getNews().subscribe(
      (data: any) => {
        this.datanews = data;
        console.log(this.datanews);
      },
      (err) => {
        console.error(err.message);
      }
    );
  }

}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
  path: '', redirectTo: 'home', pathMatch: 'full',
},
{
  path: 'home',
  loadChildren: () =>
    import('./pages/home/home.module').then((m) => m.HomeModule),
},
{
  path: 'home',
  loadChildren: () =>
    import('./pages/home/home.module').then((m) => m.HomeModule),
},
{
  path: 'match',
  loadChildren: () =>
    import('./pages/match/match.module').then((m) => m.MatchModule),
},
{
  path: 'mypet',
  loadChildren: () =>
    import('./pages/mypet/mypet.module').then((m) => m.MypetModule),
},
{
  path: 'news',
  loadChildren: () =>
    import('./pages/news/news.module').then((m) => m.NewsModule),
},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
